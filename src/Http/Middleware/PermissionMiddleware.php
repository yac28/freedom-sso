<?php

namespace Freedom\SSO\Http\Middleware;

use Closure;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Spatie\Permission\Models\Permission;

class PermissionMiddleware
{
    //protected $guard_name = config('freedom-sso.guard_name');
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }
    
        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);
    
        foreach ($permissions as $permission) {
            $check = Permission::where('name', $permission)->where('guard_name', config('freedom-sso.guard_name'))->first();
            if($check){
                if (app('auth')->user()->hasPermissionTo($permission, config('freedom-sso.guard_name'))) {
                    return $next($request);
                }
            } else {
                abort(500);
            }

        }
        return redirect('courtesy');
    }
}
