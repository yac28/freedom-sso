<?php

namespace Freedom\SSO\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            if(config('freedom-sso.cas_enable') && !config('freedom-sso.eloquent_enable'))
                return route('sso');
            else
                return route('login');
        }
    }
}
