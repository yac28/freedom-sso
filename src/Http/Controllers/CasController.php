<?php

namespace Enac\SSO\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use GuzzleHttp\Client;


class CasController extends Controller
{
    public function __construct()
    {
        $this->middleware('cas.auth');
    }

    public function validateUser(Request $request)
    {
        try{
            $client = new Client();
            
            
            $user = User::where('name',cas()->user())->first();
            $res = $client->get(config('enac-sso.ldap_ws_endpoint').cas()->user());
            if($res->getStatusCode() == '200'){
                $json = json_decode($res->getBody());
                $data = [
                    'name' => cas()->user(),
                    'email' => $json->email,
                    'first_name' => $json->nome,
                    'last_name' => $json->cognome,
                    'approved' => 1, 
                    'password' => Hash::make('SSO_password')
                ];

            }
            else{
                $data = [
                    'name' => cas()->user(),
                    'email' => 'enac.user.sso.'.Str::random(9).'@email.it',
                    'first_name' => '',
                    'last_name' => '',
                    'approved' => 1, 
                    'password' => Hash::make('SSO_password')
                ];
            }

            if(is_null($user)){
                $user = User::create($data);
            }
            else{
                $user->update($data);
            }
            
            Auth::login($user);
            return redirect('/');
        }catch(\Exception $e){
            return redirect()->back()->withErrors('');
        }
        
    }

    public function courtesy($value='')
    {
        return view('enac::cas.courtesy');
    }
}
