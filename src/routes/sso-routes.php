<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for auth of your application. These
| routes are loaded by the AuthServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => '\\Freedom\\SSO\\Http\\Controllers', 'middleware' => ['web']], function () {
	Route::get('/sso','CasController@validateUser')->name('sso');
});

Route::group(['namespace' => '\\Freedom\\SSO\\Http\\Controllers','middleware' => ['web', 'auth']], function () {
    Route::get('/courtesy','CasController@courtesy')->name('courtesy');
});