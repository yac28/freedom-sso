<?php
namespace Freedom\SSO\Classes;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class Limitless
{
	/**
	 * Crea un menu laterale suddiviso in section
	 *
	 *
	 * @param null $user Utente, se non impostato viene prese quello di default
	 * @param null $menu Elenco di Menu che si basa su percorso array annidato
	 * @param null $logged Elenco degli utenti loggati
	 * @param string $custom view custom per campiare tipo di template
	 *
	 * @return
	 */
	public function sidebar($menu, $custom = 'partials.limitless.sidebar')
	{
		return View::make($custom, compact('menu'))->render();
	}
	
	/**
	 * @param $route_name
	 *
	 * @return string
	 */
	public function navIsActive(...$routes)
	{
		foreach($routes as $route) {
				if(Str::is($route, Route::currentRouteName())) {
					return true;
				}
		}
		return false;
	}
	
	/**
	 * @param $name
	 * @param \Illuminate\Support\ViewErrorBag $errors
	 *
	 * @return \Illuminate\Config\Repository|mixed|null
	 */
	public function isValid($name, $errors = null)
	{
		if ( ! is_null( $errors ) ) {
			if ( $errors->has( $name ) ) {
				return config( 'limitless.input-danger' );
			}
		}
		return null;
	}
	
	
	/**
	 * @param $name
	 * @param \Illuminate\Support\ViewErrorBag $errors
	 * @param null $custom
	 *
	 * @return string|null
	 */
	public function alertMessage($name, $errors = null, $custom = null)
	{
		if ( ! is_null( $errors ) ) {
			if ( $errors->has( $name ) ) {
				$message = $errors->first( $name );
				return View::make( ($custom ?? config('limitless.danger-feedback-view')), compact( 'message' ) )->render();
			}
		}
		
		return null;
	}
	
	public function button()
	{
	
	}
	
	public function text()
	{
	
	}
	
	public function textarea()
	{
	
	}
}