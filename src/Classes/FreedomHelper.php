<?php

namespace Freedom\SSO\Classes;

use File;
use Illuminate\Support\Str;

class FreedomHelper
{

	public function validateMigration($name)
	{
        $files = File::allFiles(database_path('migrations'));
        foreach ($files as $file) {
           $migration = $file->getFilename();
           if(preg_match('#'.Str::snake($name).'#', $migration)){
            return false;
           } 
        }
        return true;
	}
}