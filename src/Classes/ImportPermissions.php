<?php

namespace Freedom\SSO\Classes;

class ImportPermissions
{
 	protected $permissions;

 	public function __construct()
 	{
 		$this->permissions = $this->register_permissions();
 	}
 	/**
 	Genera Array dei permessi da importare
 	**/
 	public function register_permissions()
 	{
 	      $permissions = [
        			'be.admin.access'	=> 'Permesso per accedere all\'area di Amministrazione',
					 // Utenti 
        			'be.admin.user.access'        => 'Accesso all\'elenco degli Utenti',
        			'be.admin.user.index'         => 'Elenco degli Utenti',
        			'be.admin.user.create.access' => 'Accesso alla creazione dell\'Utente',
        			'be.admin.user.edit.access'   => 'Accesso alla modifica dell\'Utente',
        			'be.admin.user.destroy.access'=> 'Accesso alla cancellazione dell\'Utente',
        			'be.admin.user.create'        => 'Creazione dell\'Utente',
        			'be.admin.user.edit'          => 'Modifica dell\'Utente',
        			'be.admin.user.destroy'       => 'Cancellazione dell\'Utente',
        			'be.admin.user.store'         => 'Salvataggio dell\'Utente',
        			'be.admin.user.update'        => 'Aggiornamento dell\'Utente',
                                //Permessi Permesso
                                'be.admin.permission.access'         => 'Accesso all\'elenco dei Permessi',
                                'be.admin.permission.index'          => 'Elenco dei Permessi',
                                'be.admin.permission.create.access'  => 'Accesso alla creazione del Permesso',
                                'be.admin.permission.edit.access'     => 'Accesso alla modifica del Permesso',
                                'be.admin.permission.destroy.access'  => 'Accesso alla cancellazione del Permesso',
                                'be.admin.permission.create'          => 'Creazione del Permesso',
                                'be.admin.permission.edit'            => 'Modifica del Permesso',
                                'be.admin.permission.destroy'         => 'Cancellazione del Permesso',
                                'be.admin.permission.store'           => 'Salvataggio del Permesso',
                                'be.admin.permission.update'          => 'Aggiornamento del Permesso',
                        
                                //Ruoli Ruolo
                                'be.admin.role.access'                => 'Accesso all\'elenco dei Ruoli',
                                'be.admin.role.index'                 => 'Elenco dei Ruoli',
                                'be.admin.role.create.access'         => 'Accesso alla creazione del Ruolo',
                                'be.admin.role.edit.access'           => 'Accesso alla modifica del Ruolo',
                                'be.admin.role.destroy.access'        => 'Accesso alla cancellazione del Ruolo',
                                'be.admin.role.create'                => 'Creazione del Ruolo',
                                'be.admin.role.edit'                  => 'Modifica del Ruolo',
                                'be.admin.role.destroy'               => 'Cancellazione del Ruolo',
                                'be.admin.role.store'                 => 'Salvataggio del Ruolo',
                                'be.admin.role.update'                => 'Aggiornamento del Ruolo',
 				 ];
 		return $permissions;
 	}
 	/**
 	Ritorn elenco dei ruoli
 	**/
 	public function get_all_permissions()
 	{
 	 return $this->permissions;
 	}
}
