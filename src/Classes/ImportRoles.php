<?php

namespace Freedom\SSO\Classes;

class ImportRoles
{
 	protected $roles;

 	public function __construct()
 	{
 		$this->roles = $this->register_roles();
 	}
 	/**
 	Genera Array dei ruoli da importare
 	**/
 	public function register_roles()
 	{
 		$roles = [
 				  ['name' => 'admin', 'description' => 'Ruolo di Amministrazione della Piattaforma'],
 				  ['name' => 'publisher', 'description' => 'Ruolo di Gestione dei Contenuti del Front end'],
 				  ['name' => 'media', 'description' => 'Ruolo di Gestione dei Media'],
 				  ['name' => 'editor', 'description' => 'Ruolo di Gestione Documenti e contenuti'],
 				  ['name' => 'writer', 'description' => 'Ruolo di Gestione dei Documenti'],
 				  ['name' => 'only_read', 'description' => 'Ruolo Assegnato Automaticamente dopo il primo accesso']
 				 ];
 		return $roles;
 	}
 	/**
 	Ritorn elenco dei ruoli
 	**/
 	public function get_all_roles()
 	{
 		return $this->roles;
 	}
}
