@extends('layouts.app')
@section('title', 'Pagina di Cortesia')
@section('page-title', 'Pagina di Cortesia')
@section('content')
    <!-- Side Bar principale -->
    <!-- Content -->
    <section id="page-content" class="col px-0 pb-4">
        <!-- include toolbar -->
    <!--CONTENUTO DELLA PAGINA-->
        <div class="px-3">
            <div class="row">
                <div class="col-12">
                    <h1 class="display-3 d-flex font-weight-semibold mb-0 min-w-0 pr-3">
                        <span class="text-truncate">Ci dispiace ma questo profilo non è abilitato a questa applicazione. Rivolgersi al supporto per maggiori informazioni.</span>
                     </h1>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
{{-- Extra CSS File --}}
@push('css')
    <style>
    </style>
@endpush
{{-- Extra JS File --}}
@push('script')
    <script>
        $(function () {
        });
    </script>
@endpush