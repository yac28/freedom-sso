<?php

return [
    // Load Routes From Package
    'use_package_routes' => true,

    'guard_name' => env('GUARD_NAME','apps_be'),

    'cas_enable' => env('CAS_ENABLE','true'),
    'eloquent_enable' => env('ELOQUENT_ENABLE','false'),
    'ldap_ws_endpoint' => env('WS_LDAP_ENDPOINT','http://10.144.0.174/OrganigrammaRestWS/rest/enac/getADUser/')

];