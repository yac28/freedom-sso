<?php

namespace Freedom\SSO;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class SSOServiceProvider extends ServiceProvider
{
    protected $namespace = 'Freedom\SSO\Http\Controllers';

    /**
     * Registra risorse indispensabili al funzionamento del package
     */
    public function register(){

    }
    /**
     * Boot il provider
     */
    public function boot()
    {
        // carica rotte
        if (config('freedom-sso.use_package_routes')) {
            $this->handleRoutes();
        }
        // carica le risorse del package
        $this->loadPackageStuff();
        // pubblica i files necessario al funzionamento del package
        $this->setpublishableResources();
        // Registra Middleware
        $this->registerMiddleWare();
        
    }
    /**
     * Wrapper per i metodi che gesiscono le rotte
     */
    private function handleRoutes()
    {
        // override Routes
        $this->app->booted(function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/sso-routes.php');
        });
    }
    /**
     * Dispone risorse aggiuntive
     */
    private function loadPackageStuff()
    {
        $this->loadViewsFrom(__DIR__.'/publishable/resources/views', 'freedom');
    }
    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function registerMiddleWare()
    {
        $router = $this->app->make(Router::class);
        // register Middleware
        $router->aliasMiddleware('permission', 'Freedom\SSO\Http\Middleware\PermissionMiddleware');
        $router->aliasMiddleware('authenticate', 'Freedom\SSO\Http\Middleware\Authenticate');
    }
    /**
     *
     */
    private function setpublishableResources()
    {
        // pubblica tutti i files necessari al package
        $this->publishes([
            __DIR__.'/publishable/config/' => config_path(),
        ], 'config');
        
        // Pubblica Viste
        $this->publishes([
            __DIR__.'/publishable/resources/views/' => resource_path('views/vendor/freedom'),
        ], 'views');
    }

}